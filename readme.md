#npm install
#apt-get install imagemagick
#apt-get install graphicsmagick
#nodemon ./bin/www

http://lori.com


413 Request Entity Too Large（请求实体太大）
https://www.jianshu.com/p/fe968f674a3c

pm2 管理nodejs 日志存放问题
https://blog.csdn.net/kane_canpower/article/details/53510239
express-mysql-session
https://www.npmjs.com/package/express-mysql-session
一个MYSQL数据库的常用操作封装，使用连续点操作，实现语义化的数据库操作。
https://www.npmjs.com/package/@hyoga/mysql


Caution This also differs from prepared statements in that all ? are replaced, even those contained in comments and strings.

Different value types are escaped differently, here is how:

Numbers are left untouched
Booleans are converted to true / false
Date objects are converted to 'YYYY-mm-dd HH:ii:ss' strings
Buffers are converted to hex strings, e.g. X'0fa5'
Strings are safely escaped
Arrays are turned into list, e.g. ['a', 'b'] turns into 'a', 'b'
Nested arrays are turned into grouped lists (for bulk inserts), e.g. [['a', 'b'], ['c', 'd']] turns into ('a', 'b'), ('c', 'd')
Objects that have a toSqlString method will have .toSqlString() called and the returned value is used as the raw SQL.
Objects are turned into key = 'val' pairs for each enumerable property on the object. If the property's value is a function, it is skipped; if the property's value is an object, toString() is called on it and the returned value is used.
undefined / null are converted to NULL
NaN / Infinity are left as-is. MySQL does not support these, and trying to insert them as values will trigger MySQL errors until they implement support.