module.exports = {
    url: 'http://localhost:3000',
    mysql: {
        host: 'localhost',
        user: 'root',
        password: 'root',
        database: 'lori',
        connectionLimit: 100,
        //timezone: '+08:00',
        dateStrings: true,
        charset: 'utf8_general_ci',
        prefix: 'szh_',
        bigNumberStrings: true,
        multipleStatements: true,
    },
    redis: {
        port: 6379, // Redis port
        host: "127.0.0.1", // Redis host
        family: 4, // 4 (IPv4) or 6 (IPv6)
        password: "",
        db: 0,
    }
}