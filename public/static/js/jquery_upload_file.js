//https://www.cnblogs.com/luyiwei/p/11163922.html
//https://www.jq22.com/webqd1852
function jquery_upload_file_one(element,url,params,callback){
	var f=$(element);
	if(!f[0].files[0]){
	  return false;	
	}

	var form = new FormData();
	form.append(f.data('name'), f[0].files[0]);
	if(params){
		for(var i in params){
			form.append(i,params[i]);
		}
	}
	//Ajax提交
	$.ajax({
	  url: url,
	  type: "POST",
	  data: form,
	  async: true,        //异步
	  processData: false,  //很重要，告诉jquery不要对form进行处理
	  contentType: false,  //很重要，指定为false才能形成正确的Content-Type
	  dataType: 'json',
	  success: function(data){
		if(callback) callback(data);
	  },
	  error: function(){
		alert('上传失败');	
	  }
	});
	return true;
}

function jquery_upload_file_many(element,url,params,callback){
	var f=$(element);
	if(!f[0].files[0]){
	  return false;	
	}

	for(var j=0;j<f[0].files.length;j++){
		var form = new FormData();
		form.append(f.data('name'), f[0].files[j]);
		if(params){
			for(var i in params){
				form.append(i,params[i]);
			}
		}
		//Ajax提交
		$.ajax({
		  url: url,
		  type: "POST",
		  data: form,
		  async: false,        //同步
		  processData: false,  //很重要，告诉jquery不要对form进行处理
		  contentType: false,  //很重要，指定为false才能形成正确的Content-Type
		  dataType: 'json',
		  success: function(data){
			if(callback) callback(data);
		  },
		  error: function(){
			alert('上传失败');	
		  }
		});
	}
	return true;
}