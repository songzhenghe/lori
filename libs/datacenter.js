var tools = require('./tools');
var config = require(tools.rootPath + 'config');
var { connection, pool, model } = require(tools.rootPath + 'libs/model');

const core_conf = require(tools.appPath + 'core_conf');
const core_enums = require(tools.appPath + 'core_enums');
var functions = require(tools.appPath + 'functions');

module.exports = class {
    _field = '*';
    _table = '';
    _where = '';
    _order = '';
    _limit = '';

    constructor() {
        this.model = model;
    }

    reset() {
        this._field = '*';
        this._table = '';
        this._where = '';
        this._order = '';
        this._limit = '';
    }
    field(v = '*') {
        this._field = v;
        return this;
    }
    table(name) {
        this._table = config.mysql.prefix + name;
        return this;
    }
    where(obj, before = '') {
        this._where = model.where(obj, before);
        return this;
    }
    order(str) {
        this._order = str;
        return this;
    }
    limit(a, b) {
        this._limit = a + ',' + b;
        return this;
    }
    async insert(data) {
        let r = await model.i(`insert into ${this._table} set ?`, data);
        this.reset();
        return r;
    }
    async update(data) {
        let r = await model.u(`update ${this._table} set ? where ${this._where}`, data);
        this.reset();
        return r;
    }
    async delete() {
        let r = await model.delete(`delete from ${this._table} where ${this._where}`);
        this.reset();
        return r;
    }
    async select() {
        let sql = '';
        sql += `select ${this._field} from ${this._table}`;
        if (this._where) sql += ` where ${this._where} `;
        if (this._order) sql += ` order by ${this._order} `;
        if (this._limit) sql += ` limit ${this._limit} `;
        let r = await model.select(sql);
        this.reset();
        return r;
    }
    async find() {
        let sql = '';
        sql += `select ${this._field} from ${this._table}`;
        if (this._where) sql += ` where ${this._where} `;
        if (this._order) sql += ` order by ${this._order} `;
        if (this._limit) sql += ` limit ${this._limit} `;
        let r = await model.find(sql);
        this.reset();
        return r;
    }
    async column(field) {
        let sql = '';
        sql += `select ${field} from ${this._table}`;
        if (this._where) sql += ` where ${this._where} `;
        if (this._order) sql += ` order by ${this._order} `;
        if (this._limit) sql += ` limit ${this._limit} `;
        let r = await model.column(sql);
        this.reset();
        return r;
    }
    async value(field) {
        let sql = '';
        sql += `select ${field} from ${this._table}`;
        if (this._where) sql += ` where ${this._where} `;
        if (this._order) sql += ` order by ${this._order} `;
        sql += ` limit 1 `;
        let r = await model.value(sql);
        this.reset();
        return r;
    }
    async map(key, value) {
        let sql = '';
        sql += `select ${key},${value} from ${this._table}`;
        if (this._where) sql += ` where ${this._where} `;
        if (this._order) sql += ` order by ${this._order} `;
        if (this._limit) sql += ` limit ${this._limit} `;
        let r = await model.map(sql);
        this.reset();
        return r;
    }

}