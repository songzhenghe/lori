//https://blog.csdn.net/weixin_41827162/article/details/85338304
//https://www.cnblogs.com/agen-su/p/11790315.html
module.exports = {
    randomNum: function (minNum, maxNum) {
        switch (arguments.length) {
            case 1:
                return parseInt(Math.random() * minNum + 1, 10);
                break;
            case 2:
                return parseInt(Math.random() * (maxNum - minNum + 1) + minNum, 10);
                //或者 Math.floor(Math.random()*( maxNum - minNum + 1 ) + minNum );
                break;
            default:
                return 0;
                break;
        }
    },
    //<%-form.text('title','title')%>
    text: function (name, val = '', id = '', cls = ['form-control']) {
        let html = `<input class="${cls.join(' ')}" type="text" name="${name}" id="${id}" value="${val}">`;
        return html;
    },
    //<%-form.textarea('textarea','你好')%>
    textarea: function (name, val = '', id = '', cls = ['form-control']) {
        let html = `<textarea class="${cls.join(' ')}" name="${name}" id="${id}" cols="50" rows="5">${val}</textarea>`;
        return html;
    },
    //<%-form.select('flag','1',{'0':'请选择','1':'显示','2':'隐藏'})%>
    select: function (name, val = '', options = {}, id = '', cls = ['form-control']) {
        let html = `<select class="${cls.join(' ')}" name="${name}" id="${id}">
        ${Object.entries(options).map((v, k) => {
            return `<option value="${v[0]}" ${val == v[0] ? 'selected' : ''}>${v[1]}</option>`;
        }).join('')}
</select>`;
        return html;
    },
    //<%-form.radio('top','0',{'1':'是','0':'否'});%>
    radio: function (name, val = '', options = {}, id = '', cls = []) {
        let html = '';
        for (let [k, v] of Object.entries(options)) {
            let id_str = id ? id + '_' + k : '';
            html += `<label><input id="${id_str}" class="${cls.join(' ')}" type="radio" name="${name}" value="${k}" ${val == k ? 'checked' : ''}> ${v}</label> `;
        }
        return html;
    },
    //<%-form.checkbox('hobby[]',3,{'1':'足球','2':'排球','3':'篮球','4':'羽毛球'});%>
    checkbox: function (name, val = [], options = {}, id = '', cls = []) {
        if (!Array.isArray(val)) val = [val];
        for (let [k, v] of val.entries()) {
            val[k] = String(v);
        }
        let html = '';
        for (let [k, v] of Object.entries(options)) {
            let id_str = id ? id + '_' + k : '';
            html += `<label><input id="${id_str}" class="${cls.join(' ')}" type="checkbox" name="${name}" value="${k}" ${val.includes(k) ? 'checked' : ''}> ${v}</label> `;
        }
        return html;
    },
    //<%-form.datetime('time1','2020-10-30 9:51:23')%>
    datetime: function (name, val = '', id = '', cls = ['form-control']) {
        let html = `<input class="${cls.join(' ')}" type="text" name="${name}" id="${id}" value="${val}" size="50" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly>`;
        return html;
    },
    //<%-form.date('date1','2020-10-30')%>
    date: function (name, val = '', id = '', cls = ['form-control']) {
        let html = `<input class="${cls.join(' ')}" type="text" name="${name}" id="${id}" value="${val}" size="50" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly>`;
        return html;
    },
    //<%-form.readonly('wjid', '100', id1 = 'wjid', val2 = '测试问卷', id2 = 'wjid_title')%>
    readonly: function (name, val1 = '', id1 = '', val2 = '', id2 = '', cls = ['form-control']) {
        let html = `<input type="hidden" name="${name}" id="${id1}" value="${val1}"> <input type="text" id="${id2}" class="${cls.join(' ')}" value="${val2}" readonly>`;
        return html;
    },
    //<%-form.editor('content','666')%>
    editor: function (name, val = '', id = '', cls = ['form-control']) {
        let html = `<textarea class="${cls.join(' ')}" name="${name}" id="${id}" rows="3">${val}</textarea>`;
        html += `
        <script>
        $(document).ready(function(e) {
            CKEDITOR.replace('${name}', {"width":600,"height":200});
        });
        </script>        
`;
        return html;
    },
    //<%-form.window('/a/b/c?flag=1', 'catid', '299', 'catid', '体育', 'catid_title')%>
    window: function (url, name, val1 = '', id1 = '', val2 = '', id2 = '', cls = ['form-control']) {
        let uniq = this.randomNum(10000, 99999);
        if (url.indexOf('?') > -1) {
            url = `${url}&callback=choose_${uniq}`;
        } else {
            url = `${url}?callback=choose_${uniq}`;
        }
        let html = this.readonly(name, val1, id1, val2, id2, cls) + ` <a class="btn btn-outline-success btn-sm choose_${uniq}" href="${url}">选择</a> <a class="btn btn-outline-danger btn-sm clear_${uniq}" href="javascript:void(0);">置空</a>`;
        html += `
        <script>
        function choose_${uniq}(v){
            $('#${id1}').val(v.itemid);
            $('#${id2}').val(v.title);
            $.colorbox.close();		
        }
        $(document).ready(function(e) {
            $('.choose_${uniq}').click(function(){
                $.colorbox({width:"600px", height:"400px", iframe:true, opacity:0.3, href: $(this).attr("href")});
                return false;
            });
            $('.clear_${uniq}').click(function(){
                if(!confirm('确定要置空吗？')) return false;
                $('#${id1}').val('');
                $('#${id2}').val('');	
            });
        });
        </script>        
`;
        return html;
    },
    //<%-form.picture('/index/form/upload',{}, 'thumb', '', 'thumb')%>
    picture: function (url, data, name, val = '', id = '', cls = ['form-control']) {
        let uniq = this.randomNum(10000, 99999);
        let html = `<input type="file" data-name="${name}" name="file_${uniq}" id="file_${uniq}" style="display:none;"> <input type="text" name="${name}" id="${id}" class="${cls.join(' ')}" value="${val}" readonly> <a class="btn btn-outline-success btn-sm choose_${uniq}" href="javascript:void(0);">选择</a> <a class="btn btn-outline-dark btn-sm preview_${uniq}" href="javascript:void(0)">预览</a> <a class="btn btn-outline-danger btn-sm clear_${uniq}" href="javascript:void(0);">置空</a>`;
        html += `
        <script>
        $(document).ready(function(e) {
            $('#file_${uniq}').change(function(){
                jquery_upload_file_one('#file_${uniq}','${url}',${JSON.stringify(data)},function(data){
                    if(data.code==0){
                        alert('上传成功');
                        $('#${id}').val(data.url);
                    }else{
                        alert(data.msg);
                    }
                });
            });
            $('.choose_${uniq}').click(function(){
                $('#file_${uniq}').trigger('click');
                return false;
            });
            $('.preview_${uniq}').click(function(){
                if(!$('#${id}').val()) return;
                $.colorbox({width:"600px", height:"400px", photo:true, opacity:0.3, href: $('#${id}').val()});
            });
            $('.clear_${uniq}').click(function(){
                if(!confirm('确定要置空吗？')) return false;
                $('#${id}').val('');
            });
        });
        </script>
        `;
        return html;
    },
    //<%-form.document('/index/form/upload',{}, 'document', '', 'document')%>
    document: function (url, data, name, val = '', id = '', cls = ['form-control']) {
        let uniq = this.randomNum(10000, 99999);
        let html = `<input type="file" data-name="${name}" name="file_${uniq}" id="file_${uniq}" style="display:none;"> <input type="text" name="${name}" id="${id}" class="${cls.join(' ')}" value="${val}" readonly> <a class="btn btn-outline-success btn-sm choose_${uniq}" href="javascript:void(0);">选择</a> <a class="btn btn-outline-dark btn-sm preview_${uniq}" href="javascript:void(0)">下载</a> <a class="btn btn-outline-danger btn-sm clear_${uniq}" href="javascript:void(0);">置空</a>`;
        html += `
        <script>
        $(document).ready(function(e) {
            $('#file_${uniq}').change(function(){
                jquery_upload_file_one('#file_${uniq}','${url}',${JSON.stringify(data)},function(data){
                    if(data.code==0){
                        alert('上传成功');
                        $('#${id}').val(data.url);
                    }else{
                        alert(data.msg);
                    }
                });
            });
            $('.choose_${uniq}').click(function(){
                $('#file_${uniq}').trigger('click');
                return false;
            });
            $('.preview_${uniq}').click(function(){
                if(!$('#${id}').val()) return;
                var a = document.createElement("a");
                a.style.display = 'none';
                a.target = '_blank';
                a.href = $('#${id}').val();
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
            });
            $('.clear_${uniq}').click(function(){
                if(!confirm('确定要置空吗？')) return false;
                $('#${id}').val('');
            });
        });
        </script>
        `;
        return html;
    },
};