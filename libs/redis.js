var fs = require('fs');
var config = require('../config');
var tools = require(__dirname + '/tools');

const Redis = require('ioredis');
module.exports = new Redis(config.redis);