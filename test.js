const datacenter = require('./libs/datacenter');

const example = new datacenter();


async function start() {
    let data = null;
    data = await example.table('article').field('itemid,title').where({ itemid: ['>', 20], flag: 1 }).order('itemid desc').limit(0, 5).select();
    console.log(data);

    data = await example.table('article').where({ itemid: ['>', 20], flag: 1 }).order('itemid desc').limit(0, 3).column('title');
    console.log(data);

    data = await example.table('article').where({ itemid: 20 }).find();
    console.log(data);

    data = await example.table('article').where({ itemid: 20 }).value('itemid');
    console.log(data);

    data = await example.table('article').where({ itemid: ['>', 20], flag: 1 }).order('itemid desc').limit(0, 3).map('itemid', 'title');
    console.log(data);

}

start();