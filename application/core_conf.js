const fs = require('fs');
const path = require('path');
var tools = require('../libs/tools');
var config = require(tools.rootPath + 'config');
var { connection, pool, model } = require(tools.rootPath + 'libs/model');

const functions = require('./functions');
module.exports = class {
    MODULE = {};
    MODULE_BYNAME = {};
    MAP = {};
    AUTH = {};
    MENU = {};
    constructor() {
        this.MD = [
            { 'moduleid': 1, 'module_en': 'admin', 'module_cn': '网站设置', 'module_dir': 'admin', 'is_auth': 1 },
            { 'moduleid': 2, 'module_en': 'member', 'module_cn': '会员管理', 'module_dir': 'member', 'is_auth': 1 },
            { 'moduleid': 11, 'module_en': 'article', 'module_cn': '文章管理', 'module_dir': 'article', 'is_auth': 1 },
        ];


        this.MODULE = this.module();
        this.MODULE_BYNAME = this.module_byname();
        this.MAP = this.map();
        this.AUTH = this.auth();
        this.MENU = this.menu();
    }

    module() {
        let MODULE = {};
        this.MD.forEach((value, index) => {
            MODULE[value['moduleid']] = value;
        });
        return MODULE;
    }

    module_byname() {
        let MODULE = {};
        this.MD.forEach((value, index) => {
            MODULE[value['module_en']] = value;
        });
        return MODULE;
    }

    map() {
        let MODULE = {};
        this.MD.forEach((value, index) => {
            MODULE[value['module_en']] = value['moduleid'];
        });
        return MODULE;
    }
    //https://www.cnblogs.com/yuer20180726/p/11377897.html
    auth() {
        let AUTH = {
            1: [
                { 'name': 'safe', 'chinese': '修改密码', 'is_auth': 0, 'note': '', 'is_menu': 1 },
                { 'name': 'setting', 'chinese': '网站设置', 'is_auth': 1, 'note': '', 'is_menu': 1 },
            ],

            2: [
                { 'name': 'add', 'chinese': '添加会员', 'is_auth': 1, 'note': '', 'is_menu': 1 },
                { 'name': 'lst', 'chinese': '会员列表', 'is_auth': 1, 'note': '', 'is_menu': 1 },
                { 'name': 'mod', 'chinese': '编辑会员', 'is_auth': 1, 'note': '', 'is_menu': 0 },
                { 'name': 'del', 'chinese': '删除会员', 'is_auth': 1, 'note': '', 'is_menu': 0 },
                { 'name': 'login_log', 'chinese': '登录日志', 'is_auth': 1, 'note': '', 'is_menu': 1 },
                { 'name': 'setting', 'chinese': '模块设置', 'is_auth': 1, 'note': '', 'is_menu': 1 },
                { 'name': 'authenticate', 'chinese': '后台权限', 'is_auth': 1, 'note': '', 'is_menu': 0 },
                { 'name': 'outside', 'chinese': '外部调用', 'is_auth': 0, 'note': '', 'is_menu': 0 },
            ],
            11: [
                { 'name': 'add', 'chinese': '添加文章', 'is_auth': 1, 'note': '', 'is_menu': 1 },
                { 'name': 'lst', 'chinese': '文章列表', 'is_auth': 1, 'note': '', 'is_menu': 1 },
                { 'name': 'mod', 'chinese': '编辑文章', 'is_auth': 1, 'note': '', 'is_menu': 0 },
                { 'name': 'del', 'chinese': '删除文章', 'is_auth': 1, 'note': '', 'is_menu': 0 },
                { 'name': 'preview', 'chinese': '查看文章', 'is_auth': 1, 'note': '', 'is_menu': 0 },
                { 'name': 'setting', 'chinese': '模块设置', 'is_auth': 1, 'note': '', 'is_menu': 1 },
                { 'name': 'outside', 'chinese': '外部调用', 'is_auth': 0, 'note': '', 'is_menu': 0 },
                { 'name': 'clear_attachments', 'chinese': '清理垃圾附件', 'is_auth': 1, 'note': '', 'is_menu': 1 },
                { 'name': 'test', 'chinese': '测试', 'is_auth': 0, 'note': '', 'is_menu': 1 },

                { 'name': 'category_add', 'chinese': '添加分类', 'is_auth': 1, 'note': '', 'is_menu': 1 },
                { 'name': 'category_mod', 'chinese': '编辑分类', 'is_auth': 1, 'note': '', 'is_menu': 0 },
                { 'name': 'category_lst', 'chinese': '分类列表', 'is_auth': 1, 'note': '', 'is_menu': 1 },
                { 'name': 'category_del', 'chinese': '删除分类', 'is_auth': 1, 'note': '', 'is_menu': 0 },
                { 'name': 'category_cache', 'chinese': '更新分类缓存', 'is_auth': 0, 'note': '', 'is_menu': 1 },
                { 'name': 'category_outside', 'chinese': '外部调用', 'is_auth': 0, 'note': '', 'is_menu': 0 },

                { 'name': 'upload_img', 'chinese': '图片上传', 'is_auth': 0, 'note': '', 'is_menu': 0 },
                { 'name': 'upload_img_action', 'chinese': '图片上传', 'is_auth': 0, 'note': '', 'is_menu': 0 },
                { 'name': 'upload_file', 'chinese': '文件上传', 'is_auth': 0, 'note': '', 'is_menu': 0 },
                { 'name': 'upload_file_action', 'chinese': '文件上传', 'is_auth': 0, 'note': '', 'is_menu': 0 },
                { 'name': 'upload_ck_img', 'chinese': 'ck图片上传', 'is_auth': 0, 'note': '', 'is_menu': 0 },
                { 'name': 'upload_ck_file', 'chinese': 'ck附件上传', 'is_auth': 0, 'note': '', 'is_menu': 0 },
                { 'name': 'upload_huge', 'chinese': '分段上传', 'is_auth': 0, 'note': '', 'is_menu': 0 },
                { 'name': 'upload_huge_action', 'chinese': '分段上传', 'is_auth': 0, 'note': '', 'is_menu': 0 },
                { 'name': 'upload_img_tradition', 'chinese': 'ajax图片上传', 'is_auth': 0, 'note': '', 'is_menu': 0 },
                { 'name': 'upload_img_tradition_action', 'chinese': 'ajax图片上传', 'is_auth': 0, 'note': '', 'is_menu': 0 },
                { 'name': 'upload_file_tradition', 'chinese': 'ajax文件上传', 'is_auth': 0, 'note': '', 'is_menu': 0 },
                { 'name': 'upload_file_tradition_action', 'chinese': 'ajax文件上传', 'is_auth': 0, 'note': '', 'is_menu': 0 },

            ],
        };
        let t = {};
        Object.keys(AUTH).forEach((k) => {
            t[k] = {};
            AUTH[k].forEach((vv) => {
                t[k][vv['name']] = vv;
            });
        });
        return t;
    }

    menu() {
        //附加菜单
        let MENU = {
            1: [

            ],

            2: [

            ],

            11: [

            ],
        };
        let t = {};
        Object.keys(MENU).forEach((k) => {
            t[k] = {};
            MENU[k].forEach((vv) => {
                t[k][vv['name']] = vv;
            });
        });

        MENU = t;

        //https://blog.csdn.net/nickzhang2016/article/details/100887080
        //https://www.cnblogs.com/xingxiangyi/p/6416468.html
        //https://www.cnblogs.com/wlxll/p/10143690.html
        t = {};
        let AUTH = this.AUTH;
        Object.keys(AUTH).forEach((k) => {
            t[k] = {};
            Object.keys(AUTH[k]).forEach((kk) => {
                let vv = AUTH[k][kk];
                if (!vv['is_menu']) {
                    return true;
                }
                t[k][vv['name']] = vv;
            });
            t[k] = functions.object_merge(t[k], MENU[k]);
        });
        return t;
    }

    mod_exists(moduleid) {
        return this.MODULE.hasOwnProperty(moduleid);
    }

    mod_single(moduleid) {
        return this.MODULE[moduleid];
    }

    mod_auth(moduleid) {
        return this.AUTH[moduleid];
    }

    mod_menu(moduleid) {
        return this.MENU[moduleid];
    }

    rule_exists(moduleid, k) {
        return this.AUTH[moduleid].hasOwnProperty(k);
    }

    rule_single(moduleid, k) {
        return this.AUTH[moduleid][k];
    }

    async user_menu(groupid, userid, menu) {
        if (groupid == 1) return menu;
        ////https://www.cnblogs.com/hepengqiang/p/9822118.html
        let t = {};

        for (var k in menu) {
            t[k] = {};
            let RIGHT = model.column("select action from szh_access_back where userid=? and moduleid=?", [userid, k]);
            Object.keys(menu[k]).forEach((kk) => {
                let vv = menu[k][kk];
                if (vv['is_auth'] && RIGHT.indexOf(vv['name']) == -1) {
                    return true;
                }
                t[k][vv['name']] = vv;
            });
        }
        return t;
    }

}