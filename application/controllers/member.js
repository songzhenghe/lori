const fs = require('fs');
const path = require('path');
var tools = require('../../libs/tools');
var config = require(tools.rootPath + 'config');
var { connection, pool, model } = require(tools.rootPath + 'libs/model');

const functions = require(tools.appPath + 'functions');

const Auth = require(tools.controllerPath + 'common/auth.js');

var md5 = require('md5');

module.exports = class extends Auth {
  async __before() {
    const flag = await super.__before();
    if (flag === false) return false;

    this.moduleid = 2;
    this.MD = this.CORE_CONF.mod_single(this.moduleid);
    functions.copy_setting(this.moduleid);
    this._setting = functions.load_setting(this.moduleid);
    this.assign({
      MODULEID: this.moduleid,
      MD: this.MD,
    });
    return true;
  }

  index(req, res) {
    res.end('member/index');
  }

  async add() {
    let post = this.post();
    if (this.isPost && Object.keys(post).length > 0) {
      post = functions.dtrim(post);
      let username = post['username'];
      if (!username) {
        this.msg(0, '账号不能为空');
        return;
      }
      let password = post['password'];
      if (!password) {
        this.msg(0, '密码不能为空');
        return;
      }
      let e = await model.value("select userid from szh_member where username=?", [username]);
      if (e) {
        this.msg(0, '账号已经存在');
        return;
      }

      post['password'] = md5(post['password']);

      let insertid = await model.i("insert into szh_member set ?", post);
      this.msg(insertid);
      return;
    }
    this.assign({
      groupid: 0
    })
    return this.tpl();
  }

  async lst() {
    //https://www.cnblogs.com/cheng-du-lang-wo1/p/7259343.html
    let get = this.get();
    var { userid, username, groupid, truename } = get;

    let where = {};
    if (typeof (userid) == 'undefined') userid = '';
    if (userid) {
      where['userid'] = userid;
    }

    if (typeof (username) == 'undefined') username = '';
    if (username) {
      where['username'] = username;
    }

    if (typeof (groupid) == 'undefined') groupid = 0;
    if (groupid > 0) {
      where['groupid'] = groupid;
    }

    if (typeof (truename) == 'undefined') truename = '';
    if (truename) {
      where['truename'] = ['like', `%${truename}%`];
    }
    let wh = model.where(where, '1=1');
    let page = typeof (get.page) != 'undefined' ? get.page : 1;
    let pagesize = parseInt(this._setting['pagesize']);
    let total = await model.value(`select count(*) from szh_member where ${wh}`);
    let data = await model.select(`select * from szh_member where ${wh} order by userid desc limit ?,?`, [(page - 1) * pagesize, pagesize]);
    let pager = functions.pagination(this.REQUEST_URI, get, page, pagesize, total);

    this.assign({
      userid, username, groupid, truename, data, pager, functions
    });
    return this.tpl();
  }

  async mod() {
    let get = this.get();
    let userid = parseInt(get.userid);
    if (!userid) {
      this.msg(0);
      return;
    }
    let post = this.post();
    if (this.isPost && Object.keys(post).length > 0) {
      if (!post['password']) {
        delete post['password'];
      } else {
        post['password'] = md5(post['password']);
      }
      let r = await model.u("update szh_member set ? where userid=?", post, [userid]);
      this.msg(r, '', post['FORWARD']);
      return;
    }
    let data = await model.find("select * from szh_member where userid=?", [userid]);
    if (typeof (data['userid']) == 'undefined') {
      this.msg(0, '数据不存在');
      return;
    }
    this.assign({
      data,
    });
    return this.tpl();
  }

  async del() {
    let get = this.get();
    let userid = parseInt(get.userid);

    if (!userid) {
      this.msg(0);
      return;
    }
    let data = await model.find("select * from szh_member where userid=?", [userid]);
    if (typeof (data['userid']) == 'undefined') {
      this.msg(0, '数据不存在');
      return;
    }
    let r = model.update("update szh_member set groupid=? where userid=?", [groupid, userid]);
    this.msg(r);
    return;
  }

  async outside() {
    let get = this.get();
    var { callback, userid, username, groupid, truename } = get;

    if (typeof (callback) == 'undefined') callback = '';
    if (!callback) {
      this.msg(0, 'lose callback');
      return;
    }

    let where = {};
    if (typeof (userid) == 'undefined') userid = '';
    if (userid) {
      where['userid'] = userid;
    }

    if (typeof (username) == 'undefined') username = '';
    if (username) {
      where['username'] = username;
    }

    if (typeof (groupid) == 'undefined') groupid = 0;
    if (groupid > 0) {
      where['groupid'] = groupid;
    }

    if (typeof (truename) == 'undefined') truename = '';
    if (truename) {
      where['truename'] = ['like', `%${truename}%`];
    }
    let wh = model.where(where, '1=1');
    let page = typeof (get.page) != 'undefined' ? get.page : 1;
    let pagesize = parseInt(this._setting['pagesize']);
    let total = await model.value(`select count(*) from szh_member where ${wh}`);
    let data = await model.select(`select * from szh_member where ${wh} order by userid desc limit ?,?`, [(page - 1) * pagesize, pagesize]);
    let pager = functions.pagination(this.REQUEST_URI, get, page, pagesize, total);

    this.assign({
      callback, userid, username, groupid, truename, data, pager, functions
    });
    return this.tpl();
  }

  async login_log() {
    let get = this.get();
    var { username, loginip, logintime_1, logintime_2 } = get;

    let where = {};
    if (username) {
      where['username'] = username;
    }
    if (loginip) {
      where['loginip'] = loginip;
    }
    if (logintime_1) {
      where['logintime_1'] = ['exp', ` logintime >='${functions.strtotime(logintime_1)}' `];
    }
    if (logintime_2) {
      where['logintime_2'] = ['exp', ` logintime <='${functions.strtotime(logintime_2 + ' 23:59:59')}' `];
    }

    let wh = model.where(where, '1=1');
    let page = typeof (get.page) != 'undefined' ? get.page : 1;
    let pagesize = parseInt(this._setting['pagesize']);
    let total = await model.value(`select count(*) from szh_login_back where ${wh}`);
    let data = await model.select(`select * from szh_login_back where ${wh} order by itemid desc limit ?,?`, [(page - 1) * pagesize, pagesize]);
    let pager = functions.pagination(this.REQUEST_URI, get, page, pagesize, total);

    this.assign({
      username, loginip, logintime_1, logintime_2, data, pager, functions
    });
    return this.tpl();
  }

  async setting() {
    let post = this.post();
    if (this.isPost && Object.keys(post).length > 0) {
      let r = functions.save_setting(this.moduleid, post);
      this.msg(r);
      return;
    }
    let setting = functions.load_setting(this.moduleid);
    this.assign({
      setting: setting,
    })
    return this.tpl();
  }

  async authenticate() {
    let get = this.get();
    var { userid, moduleid } = get;
    userid = parseInt(userid);
    if (!userid) {
      this.msg(0, 'lose userid');
      return;
    }
    if (typeof (moduleid) == 'undefined') moduleid = 1;
    moduleid = parseInt(moduleid);

    let module_list = this.CORE_CONF.MODULE;
    if (!functions.in_object(moduleid, module_list)) {
      this.msg(0, '非法操作');
      return;
    }

    let access_back_model = this.model('access_back');

    let post = this.post();
    if (this.isPost && Object.keys(post).length > 0) {
      let r = await access_back_model.set_auth(userid, post);
      this.msg(r);
      return;
    }

    let mod = this.CORE_CONF.mod_single(moduleid);
    let checked = await access_back_model.get_auth(userid, moduleid);

    this.assign({
      userid, moduleid, module_list, mod, checked, functions
    })
    return this.tpl();
  }

};
