const fs = require('fs');
const path = require('path');
var tools = require('../../libs/tools');
var config = require(tools.rootPath + 'config');
var { connection, pool, model } = require(tools.rootPath + 'libs/model');

const Auth = require(tools.controllerPath + 'common/auth.js');
const functions = require(tools.appPath + 'functions');

const os = require('os');
module.exports = class extends Auth {
  constructor() {
    super();
  }
  async __before() {
    return await super.__before();
  }
  async index() {
    let mysql_version = await model.value('select VERSION() as version');
    this.assign({
      os: os.type(),
      time: functions.date(),
      version: '1.0',
      mysql_version: mysql_version,
    });
    return this.tpl();
  }

  async logout(req, res) {
    await this.req.session.destroy();
    this.redirect('/welcome/login');
  }

  async set_menu_current() {
    let post = this.post();
    await this.session('open_sidebar_menu', post.a);
    await this.session('open_sidebar_item', post.href);
    this.json({ result: functions.time() });
    return;
  }

  async get_menu_current() {
    let a = await this.session('open_sidebar_menu');
    if (!a) a = 0;
    let href = await this.session('open_sidebar_item');
    if (!href) href = '#';
    this.json({
      result: {
        a: a,
        href: href,
      }
    });
    return;
  }

};
