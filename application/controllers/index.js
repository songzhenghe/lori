const fs = require('fs');
const path = require('path');
var tools = require('../../libs/tools');
var config = require(tools.rootPath + 'config');
var { connection, pool, model } = require(tools.rootPath + 'libs/model');

var base = require(tools.controllerPath + 'common/base');
const functions = require(tools.appPath + 'functions');
var captcha = require('hahoo-captcha');

module.exports = class extends base {
  constructor() {
    super();
  }
  index(req, res) {
    res.redirect('/welcome/login');
  }
  async code(req, res) {
    let data = await captcha.default.toBuffer();
    //console.log(data);
    req.session.CODE = data.text;
    res.header('Content-type', 'image/png');
    res.end(data.buffer);
  }

}
