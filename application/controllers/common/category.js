const fs = require('fs');
const path = require('path');
var tools = require('../../../libs/tools');
var config = require(tools.rootPath + 'config');
var { connection, pool, model } = require(tools.rootPath + 'libs/model');

const Auth = require(tools.controllerPath + 'common/auth');
const functions = require(tools.appPath + 'functions');

module.exports = class extends Auth {

    async __before() {
        const flag = await super.__before();
        if (flag === false) return false;

        this.assign({
            CATEGORY_CACHE_URL: `/${this.MM}/category_cache`,
            CATEGORY_MOD_URL: `/${this.MM}/category_mod`,
            CATEGORY_DEL_URL: `/${this.MM}/category_del`,
        });
        return true;
    }

    async category_add() {

        let post = this.post();
        let category_model = this.model('category');
        if (!tools.isEmpty(post)) {
            post = functions.dtrim(post);

            post['moduleid'] = this.moduleid;
            post['listorder'] = parseInt(post['listorder']);
            if (tools.isEmpty(post['moduleid'])) {
                this.msg(0, 'moduleid不能为空');
                return;
            }
            if (tools.isEmpty(post['catname'])) {
                this.msg(0, '分类名称不能为空');
                return;
            }
            let e = await model.find("select * from szh_category where moduleid=? and parentid=? and catname=?", [post.moduleid, post.parentid, post.catname]);
            if (!tools.isEmpty(e)) {
                this.msg(0, '分类名称已经存在');
                return;
            }
            let insertid = await model.insert("insert into szh_category set ?", post);
            await category_model.cache(post.moduleid);
            this.msg(insertid);
            return;
        }

        let parent = await category_model.category_select(this.moduleid, 'parentid', 0, '顶级分类', ' class="form-control" ');
        let max_catid = await category_model.max_catid() + 1;

        this.assign({
            parent, max_catid,
        });
        return this.tpl('category/category_add');
    }

    async category_lst() {
        let get = this.get();

        let cond = {};
        cond['moduleid'] = this.moduleid;
        let wh = model.where(cond);

        let category_model = this.model('category');
        let category_arr = await model.select(`select * from szh_category where ${wh}`);
        category_arr = category_model.getCatTree(category_arr);

        this.assign({
            category_arr,
            functions,
        });
        return this.tpl('category/category_lst');
    }

    async category_mod() {
        let get = this.get();
        var { catid } = get;
        catid = parseInt(catid);
        if (!catid) {
            this.msg(0, 'lose catid');
            return;
        }
        let post = this.post();
        let category_model = this.model('category');
        if (!tools.isEmpty(post)) {
            post = functions.dtrim(post);

            post['moduleid'] = this.moduleid;
            post['listorder'] = parseInt(post['listorder']);
            if (tools.isEmpty(post['moduleid'])) {
                this.msg(0, 'moduleid不能为空');
                return;
            }
            if (tools.isEmpty(post['catname'])) {
                this.msg(0, '分类名称不能为空');
                return;
            }
            let e = await model.find("select * from szh_category where catid!=? and moduleid=? and parentid=? and catname=?", [catid, post.moduleid, post.parentid, post.catname]);
            if (!tools.isEmpty(e)) {
                this.msg(0, '分类名称已经存在');
                return;
            }
            let r = await category_model.mod_check_ok(this.moduleid, catid, post['parentid']);
            if (!r) {
                this.msg(0, '上级分类选择错误');
                return;
            }
            let r2 = await model.u("update szh_category set ? where catid=?", post, [catid]);
            await category_model.cache(this.moduleid);
            this.msg(r2, '', post['FORWARD']);
            return;
        }

        let data = await model.find("select * from szh_category where moduleid=? and catid=?", [this.moduleid, catid]);
        if (tools.isEmpty(data)) {
            this.msg(0, '分类不存在');
            return;
        }
        let parent = await category_model.category_select(this.moduleid, 'parentid', data['parentid'], '顶级分类', ' class="form-control" ');

        this.assign({
            parent, data,
        });
        return this.tpl('category/category_mod');
    }

    async category_del() {
        let get = this.get();
        var { catid } = get;
        catid = parseInt(catid);
        if (!catid) {
            this.msg(0, 'lose catid');
            return;
        }
        let category_model = this.model('category');
        let r = await category_model.del_check_ok(catid, this.moduleid);
        if (!r) {
            this.msg(0, '该分类无法删除，请先删除其子分类，并删除其下文章');
            return;
        }
        let cat = await model.find("select * from szh_category where moduleid=? and catid=?", [this.moduleid, catid]);
        if (!cat) {
            this.msg(0, '数据不存在');
            return;
        }
        let r2 = await model.delete("delete from szh_category where catid=?", [catid]);
        await category_model.cache(this.moduleid);
        this.msg(r2);
        return;
    }

    async category_cache() {
        let model = this.model('category');
        await model.cache(this.moduleid);
        this.msg(1);
        return;
    }

    async category_outside() {
        let get = this.get();

        var { callback } = get;
        if (typeof (callback) == 'undefined') callback = '';
        if (!callback) {
            this.msg(0, 'lose callback');
            return;
        }

        let cond = {};
        cond['moduleid'] = this.moduleid;
        let wh = model.where(cond);

        let category_model = this.model('category');
        let category_arr = await model.select(`select * from szh_category where ${wh}`);
        category_arr = category_model.getCatTree(category_arr);

        this.assign({
            callback,
            category_arr,
            functions,
        });
        return this.tpl('category/category_outside');
    }
};