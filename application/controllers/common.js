const fs = require('fs');
const path = require('path');
var tools = require('../../libs/tools');
var config = require(tools.rootPath + 'config');
var { connection, pool, model } = require(tools.rootPath + 'libs/model');

const Auth = require(tools.controllerPath + 'common/auth.js');
const functions = require(tools.appPath + 'functions');

module.exports = class extends Auth {
  async __before() {
    return await super.__before();
  }
  index(req, res) {
    res.end('common/index');
  }


};
