var tools = require('../../libs/tools');
var config = require(tools.rootPath + 'config');
var { connection, pool, model } = require(tools.rootPath + 'libs/model');

const datacenter = require(tools.rootPath + 'libs/datacenter');

module.exports = class extends datacenter {

    //后台权限
    async set_auth(userid, post) {
        /*
{
  moduleid: '2',
  module: 'member',
  'access': [ 'add', 'lst', 'mod', 'del' ]
}
        */
        //勾选一个时需要转成数组,不勾选时变时undefined
        console.log(post);
        if (typeof post['access'] == 'undefined') post['access'] = [];
        if (typeof (post['access']) == 'string') post['access'] = [post['access']];

        await model.delete("delete from szh_access_back where userid=? and moduleid=?", [userid, post['moduleid']]);

        for (let i = 0; i < post['access'].length; i++) {
            await model.i("insert into szh_access_back set ?", {
                userid: userid,
                moduleid: post['moduleid'],
                module: post['module'],
                controller: post['module'],
                action: post['access'][i],
            });
        }

        return true;
    }

    async get_auth(userid, moduleid) {
        let data = await model.select("select * from szh_access_back where userid=? and moduleid=?", [userid, moduleid]);
        let ret = {};
        data.forEach((v, k) => {
            if (typeof (ret[v['controller']]) == 'undefined') {
                ret[v['controller']] = [];
            }
            ret[v['controller']].push(v['action']);
        });
        return ret;
    }
};
