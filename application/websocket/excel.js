module.exports = {
    index: function (io, client, data) {
        console.log('excel/index', data);
        client.emit('communication', {
            words: 'excel/index',
        });
    },
    export: function (io, client, data) {
        console.log('excel/export', data);
        client.emit('communication', {
            words: 'excel/export',
        });
        //导出操作需要很长时间
        setTimeout(() => {
            client.emit('communication', {
                url: '/xlsxs/szh_article.xlsx',
                callback: data.callback,
            });
        }, 10000);
    }
}