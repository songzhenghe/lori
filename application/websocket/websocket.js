module.exports = {
    _init: function (io) {
        //定时广播
        let i = 1;

        setInterval(function () {
            io.emit('communication', {
                words: `this is the ${i} broadcast!`,
            });
            i++;
        }, 10000);
    },
    _connection: function (io, client) {
        console.log('new user come!');

    },
    _disconnect: function (io) {
        console.log('disconnect');
    },

    ping: function (io, client, data) {
        client.emit('communication', {});
    }

};