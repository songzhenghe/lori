module.exports = {

    index: function (io, client, data) {
        console.log('receive data:', data);
        client.emit('communication', {
            time: new Date().getTime(),
        });
    },
    hello: function (io, client, data) {
        console.log('say hello');
        client.emit('communication', {
            words: 'hello',
        });
    },
    //收到消息进行广播
    news: function (io, client, data) {
        io.emit('communication', data);
    }

};