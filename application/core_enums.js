module.exports = {
    flag: {
        1: '显示',
        2: '隐藏',
        3: '删除',
    },
    member_group: {
        1: '超级管理员',
        2: '普通管理员',
        3: '禁止访问',
    }
}