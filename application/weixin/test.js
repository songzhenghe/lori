var express = require('express');
var router = express.Router();

var tools = require('../../libs/tools');
var config = require(tools.rootPath + 'config');
var { connection, pool, model } = require(tools.rootPath + 'libs/model');

var wechat = require('wechat');
var wechat_reply = require(tools.appPath + 'weixin/index.js');

router.post('/', wechat({
    token: 'szh2020',
    appid: 'wxe9be79441cee423e',
    encodingAESKey: '',
    checkSignature: false // 可选，默认为true。由于微信公众平台接口调试工具在明文模式下不发送签名，所以如要使用该测试工具，请将其设置为false
}, function (req, res, next) {
    wechat_reply.index(req, res);
}));

module.exports = router;